package persistence;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ModuleDAO {

    public Module create(Module m) {
        Connection cnx = JDBCUtil.getInstance();
        try {
            PreparedStatement ps =
                    cnx.prepareStatement("INSERT INTO module (cour ,prof) VALUES(?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, m.getCour());
            ps.setString(2, m.getProf());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return m;
    }

    public Module update(Module m) {
        Connection cnx = JDBCUtil.getInstance();
        try {
            PreparedStatement ps =
                    cnx.prepareStatement("UPDATE module set cour=? ,prof=? where idModule = ?");
            ps.setString(1, m.getCour());
            ps.setString(2, m.getProf());
            ps.setInt(3, m.getIdMatiere());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return m;
    }

    public Module read(int idModule) {
        Connection cnx = JDBCUtil.getInstance();
        try {
            PreparedStatement ps = cnx.prepareStatement("SELECT * FROM module WHERE idModule = ?");
            ps.setInt(1, idModule);
            ResultSet result = ps.executeQuery();
            if (result.next())
                return new Module(result.getInt(1), result.getString(2), result.getString(3));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public Module[] readAll() {
        try {
            List<Module> modules = new ArrayList<>();
            Connection cnx = JDBCUtil.getInstance();
            Statement stmt = cnx.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM module");
            while (result.next())
                modules.add(new Module(result.getInt(1), result.getString(2), result.getString(3)));
            Module[] modulesArray = new Module[modules.size()];

            return modules.toArray(modulesArray);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean delete(int id) {
        try {
            Connection cnx = JDBCUtil.getInstance();
            PreparedStatement ps = cnx.prepareStatement("DELETE FROM module WHERE idModule = ?");
            ps.setInt(1, id);
            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}

