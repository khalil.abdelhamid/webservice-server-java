package persistence;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;



@XmlRootElement
public class Enseignant implements Serializable {
    private static final long serialVersionUID = 1L;
    private String cin;
    private String nom;
    private String prenom;
    private Integer age;
    private String adresse;
    private String ville;
    private String sexe;
    private String photo;

    public Enseignant() {
    }

    public Enseignant(String cin, String nom, String prenom, Integer age, String adresse, String ville, String sexe, String photo) {
        this.cin = cin;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.adresse = adresse;
        this.ville = ville;
        this.sexe = sexe;
        this.photo = photo;
    }


    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
