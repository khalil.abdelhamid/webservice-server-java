package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCUtil {

    private final static String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static Connection connection;

    static {
        try {
            String url = "jdbc:mysql://localhost:3306/ecole";
            String username = "root";
            String password = "root";
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(true);
            System.out.println("Connection established");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private JDBCUtil() {
    }

    public static Connection getInstance() {
        return connection;
    }
}
