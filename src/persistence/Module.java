package persistence;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
public class Module implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idModule;
    private String cour;
    private String prof;

    public Module() {
    }

    public Module(Integer idModule, String cour, String prof) {
        this.idModule = idModule;
        this.cour = cour;
        this.prof = prof;
    }

    public Integer getIdMatiere() {
        return idModule;
    }

    public void setIdMatiere(Integer idModule) {
        this.idModule = idModule;
    }

    public String getCour() {
        return cour;
    }

    public void setCour(String cour) {
        this.cour = cour;
    }

    public String getProf() {
        return prof;
    }

    public void setProf(String prof) {
        this.prof = prof;
    }

    @Override
    public String toString() {
        return "Matiere{" +
                "idModule=" + idModule +
                ", cour='" + cour + '\'' +
                ", prof=" + prof +
                '}';
    }
}
