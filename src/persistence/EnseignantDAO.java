package persistence;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EnseignantDAO {

    public Enseignant create(Enseignant p) {
        Connection cnx = JDBCUtil.getInstance();
        try {
            PreparedStatement ps =
                    cnx.prepareStatement("INSERT INTO enseignants (cin, nom ,prenom ,adresse,age,ville, sexe, photo) VALUES(?, ?, ?, ?,?, ?, ?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, p.getCin());
            ps.setString(2, p.getNom());
            ps.setString(3, p.getPrenom());
            ps.setString(4, p.getAdresse());
            ps.setInt(5, p.getAge());
            ps.setString(6, p.getVille());
            ps.setString(7, p.getSexe());
            ps.setString(8, p.getPhoto());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return p;
    }

    public Enseignant read(String cin) {
        Connection cnx = JDBCUtil.getInstance();
        try {
            PreparedStatement ps = cnx.prepareStatement("SELECT * FROM enseignants WHERE cin = ?");
            ps.setString(1, cin);
            ResultSet result = ps.executeQuery();
            if (result.next())
                return new Enseignant(
                        result.getString("cin"),
                        result.getString("nom"),
                        result.getString("prenom"),
                        result.getInt("age"),
                        result.getString("adresse"),
                        result.getString("ville"),
                        result.getString("sexe"),
                        result.getString("photo")
                );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public Enseignant[] getAll() {
        try {
            List<Enseignant> enseignantss = new ArrayList<>();
            Connection cnx = JDBCUtil.getInstance();
            Statement stmt = cnx.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM enseignants");
            while (result.next())
                enseignantss.add(new Enseignant(
                        result.getString("cin"),
                        result.getString("nom"),
                        result.getString("prenom"),
                        result.getInt("age"),
                        result.getString("adresse"),
                        result.getString("ville"),
                        result.getString("sexe"),
                        result.getString("photo")
                ));
            
            Enseignant[] ens = new Enseignant[enseignantss.size()];
            
            return enseignantss.toArray(ens);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean update(Enseignant p, String cin) {
        try {
            Connection cnx = JDBCUtil.getInstance();
            PreparedStatement ps = cnx.prepareStatement("UPDATE enseignants SET nom = ?,prenom=?,age=?,adresse=?,ville=?,sexe=?,photo=? WHERE cin = ?");
            ps.setString(1, p.getNom());
            ps.setString(2, p.getPrenom());
            ps.setInt(3, p.getAge());
            ps.setString(4, p.getAdresse());
            ps.setString(5, p.getVille());
            ps.setString(6, p.getSexe());
            ps.setString(7, p.getPhoto());
            ps.setString(8, cin);
            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Boolean delete(String cin) {
        try {
            Connection cnx = JDBCUtil.getInstance();
            PreparedStatement ps = cnx.prepareStatement("DELETE FROM enseignants WHERE cin = ?");
            ps.setString(1, cin);
            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


}
