package services;
import persistence.ModuleDAO;
import persistence.Module;


public class ModuleService {

  private ModuleDAO matiereDao = new ModuleDAO();

  public ModuleService() {
  }


  public Module addModule(Module mat) {
    return this.matiereDao.create(mat);
  }

  public Module updateModule(int id, Module mat) {
    if (this.matiereDao.update(mat) != null) {
      return mat;
    }
    return null;
  }

  public boolean deleteModule(int id) {
    return this.matiereDao.delete(id);
  }

  public Module[] getmats() {
    return this.matiereDao.readAll();
  }
}
