package services;
import persistence.EnseignantDAO;
import persistence.Enseignant;


public class ProfService {
  private EnseignantDAO profDAO = new EnseignantDAO();

  public ProfService() {
  }

  public Enseignant addProf(Enseignant professeur) {
    return this.profDAO.create(professeur);
  }

  public Enseignant updateProf(String cin, Enseignant professeur) {
    if (this.profDAO.update(professeur, cin)) {
      return professeur;
    }
    return null;
  }

  public boolean deleteProf(String cin) {
    return this.profDAO.delete(cin);

  }

  public Enseignant[] getprofesseurs() {
    return this.profDAO.getAll();
  }

  public Enseignant getEnseignant(String cin) {
    return this.profDAO.read(cin);
  }

}
